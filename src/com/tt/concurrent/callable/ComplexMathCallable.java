package com.tt.concurrent.callable;

import java.util.concurrent.Callable;

public class ComplexMathCallable implements Callable<Double> {
    private ComplexMath cm;
    private int threadNumber;
    private int numberOfThreads;

    public ComplexMathCallable(ComplexMath cm, int threadNumber, int numberOfThreads) {
        this.cm = cm;
        this.threadNumber = threadNumber;
        this.numberOfThreads = numberOfThreads;
    }

    @Override
    public Double call() throws Exception {
        return cm.calculate(threadNumber, numberOfThreads);
    }

    public int getThreadNumber() {
        return threadNumber;
    }

    public void setThreadNumber(int threadNumber) {
        this.threadNumber = threadNumber;
    }

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

}
