package com.tt.concurrent.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 1. Stwórz pulę wątków
 * a) Wykorzystaj Runtime.getRuntime().availableProcessors() aby pobrać ilość dostępnych procesów
 * b) pulę stwórz wykorzystując Executors.newFixedThreadPool(numberOfThreads);
 * 2. Stwórz zadania ComplexMathCallable w zależności od ilości wątków
 * 3. Zgłoś zadania do wykonania (metoda submit);
 * 4. Odłóż zwracane obiekty typu Future na listę - przydadzą się przy pobieraniu wyników
 * 5. Przeiteruj po liście i wywołaj metodę get() na obiektach Future
 * a) pamiętaj, że poszczególne wyniki należy zsumować
 * 6. Pamiętaj o zamknięciu ExecutorService'u - executor.shutdown();
 * 7. Zaprezentuj wyniki i zinterpretuj czas wykonania
 */
public class ComplexMathRunner {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("Program start...");

        int availableProcessors = Runtime.getRuntime().availableProcessors();
        int numberOfThreads = availableProcessors * 2;

        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        List<Future<Double>> futures = new ArrayList<>();

        ComplexMath cm = new ComplexMath(8000, 8000);

        long startTime = System.currentTimeMillis();

        for (int i = 1; i <= numberOfThreads; i++) {
            ComplexMathCallable callable = new ComplexMathCallable(cm, i, numberOfThreads);
            Future<Double> future = executorService.submit(callable);
            futures.add(future);
        }

        double sum = 0.0;
        for (Future<Double> future : futures) {
            sum += future.get();
        }
        System.out.println(sum);
        long endTime = System.currentTimeMillis();

        executorService.shutdown();

        System.out.println("Execution time: " + ((endTime - startTime) / 1000d) + " seconds.");


        //Podstawowy program
        System.out.println("Program start...");
        ComplexMath cm1 = new ComplexMath(8000, 8000);
        startTime = System.currentTimeMillis();
        System.out.println(cm.calculate(1,1));
        endTime = System.currentTimeMillis();
        System.out.println("Execution time: " + ((endTime - startTime) / 1000d) + " seconds.");
    }
}
